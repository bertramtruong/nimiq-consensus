# Nimiq Consensus

This repository contains a snapshot of the full consensus database of the Nimiq network to speed up the initial sync for full nodes.

If you don't know what this is, you most likely shouldn't need it.

Even, if you do know what this is -- you also most likely **shouldn't use it** as it's not officially endorsed by the Nimiq Network.

## USE AT YOUR OWN RISK.
